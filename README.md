# FALSE language compiler #

### Description ###
* [FALSE language](https://esolangs.org/wiki/FALSE) compiler written in Python.
* Current version is 0.1

### Installation ###

* You will need `nasm` assembler, `gcc` and `python2` 
* The compiler itself is `compiler.py`
* If you changed something, you can always check whether you've spoiled 
  something running `tester.py` (the only argument is the path to folder with tests - 
  `tests` by default )

### Contribution guidelines ###

You can help me to support the compiler by:

* Writing more tests (the description of `tests` folder structure  is
  in `tests/description.txt` )
