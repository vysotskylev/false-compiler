class Assembler:
    def __init__(self, outFile):
        self.CALL_STACK_SIZE = 10000
        self.funCounter = 0
        self.of = open(outFile, "w")
        self.s = ''
        self.labelNum = 0
        self.indent = 0
        self.strings = []
    def genLabel(self):
        self.labelNum += 1
        return 'L' + str(self.labelNum)
    def getIncludes(self):
        return '%include "io.inc"\n'
    def getDataSection(self):
        s = 'SECTION .data\n  call_stack_ptr: dd call_stack\n  saved_esp: dd 0\n' 
        for i,s1 in enumerate(self.strings):
            s += '  str{} : db "{}",0 \n'.format(i,s1)
        s += 'SECTION .bss\n  storage: resd 26\n  call_stack: resd ' +str( self.CALL_STACK_SIZE )+ '\n'
        return s
    def getTextSectionHeader(self):
        return 'SECTION .text\n' 
    def execLexem(self, l):
        t, v = l.typ, l.val

        #------------ARITHMETICAL---------------
        if   t == '+':
            self.append("pop eax", "add dword  [esp], eax")
        elif t == '-':
            self.append("pop eax", "sub dword  [esp], eax")
        elif t == '*':
            self.append("pop eax", "imul dword  [esp]", "mov dword  [esp], eax")
        elif t == '/':
            self.append("pop ebx", "pop eax", "cdq", "idiv ebx", "push eax")
        elif t == '_':
            self.append("neg dword [esp]")

        # ---------LOGICAL-----------
        elif t == '=':
            self.append("mov edx, -1", "mov ecx, 0", "pop eax", "pop ebx", "cmp eax, ebx", "cmove ecx, edx", "push ecx")
        elif t == '>':
            self.append("mov edx, -1", "mov ecx, 0", "pop eax", "pop ebx", "cmp ebx, eax", "cmovg ecx, edx", "push ecx")
        elif t == '|':
            self.append("pop eax", "or dword[esp], eax")
        elif t == '&':
            self.append("pop eax", "and dword[esp], eax")
        elif t == '~':
            self.append("not dword[esp]")
        
        #-----------STACK------------
        elif t == '$':
            self.append("push dword[esp]")
        elif t == '%':
            self.append("pop eax")
        elif t == '\\':
            self.append("pop eax", "pop ebx", "push eax", "push ebx")
        elif t == '@':
            self.append("pop eax", "pop ebx", "pop ecx", "push ebx", "push eax", "push ecx")
        elif t == 'O':
            self.append("pop eax", "push dword [esp + 4*eax]")

        #-------------VARIABLES-------------
        elif t == 'number':
            self.append("push " + str(v))
        elif t == 'char':
            self.append("push " + str(ord(v)))
        elif t == 'varSet':
            self.append("pop eax", "mov dword[storage + 4 * " + str(ord(v) - ord('a')) + "], eax")
        elif t == 'varGet':
            self.append("mov eax, dword[storage + 4*" + str(ord(v) - ord('a')) + "]", "push eax")
        
        #-------------FUNCTIONS---------------
        elif t == 'fun':
            # v = index in function table
            self.append("push fun" + str(v) )
        elif t == '!':
            self.append("pop eax")
            self.genFunCall("eax")
            
        #---------------CONTROL   FLOW-------------------
        elif t == '?':  
            label = self.genLabel()
            self.append("pop edx", "pop ecx", "test ecx, ecx", "jz " + label)
            self.append("add dword[call_stack_ptr], 4", "mov esi, dword[call_stack_ptr]")
            self.append("mov dword[esi], " + label, "jmp edx", label + ": nop")
        elif t == '#':
            self.append("pop eax", "pop ebx")
            # Now `eax` = body, `ebx` = condition
            bodyLabel = self.genLabel()
            condLabel = self.genLabel()
            self.append("jmp " + condLabel)
            self.append(bodyLabel + ": nop")
            self.incIndent()
            self.genFunCall("eax", ["eax", "ebx"])
            self.decIndent()

            self.append(condLabel + ": nop")
            self.incIndent()
            self.genFunCall("ebx", ["eax", "ebx"])
            self.decIndent()
            self.append("pop ecx", "test ecx, ecx", "jnz " + bodyLabel)
       
        #-------------INPUT/OUTPUT--------------
        elif t == '.':
            self.append("PRINT_DEC 4, [esp]", "pop eax")
        elif t == ',':
            self.append("PRINT_CHAR [esp]", "pop eax")
        elif t == '^':
            self.append("GET_CHAR eax", "push eax")
        elif t == 'str':
            self.append("PRINT_STRING str{}".format(v))
        elif t == 'B' :
            raise Exception("Flush operation(B) is not implemented as streams are not buffered")
        else:
            raise Exception("Unknown lexem: {" + l.toString() + "}")
    
    def addMain(self, lexems):
        self.append('global CMAIN', 'CMAIN:')
        self.incIndent()
        self.append( "mov eax, esp", "mov [saved_esp], eax")
        for lex in lexems:
            self.execLexem(lex)
        self.append("xor eax, eax", "mov esp, [saved_esp]", "ret")
        self.decIndent()
    def addFunctions(self, funs):
        self.funCounter = len(funs)
        for fi, fun in enumerate(funs):
            self.append("fun" + str(fi)+ ":")
            self.incIndent()
            for lex in fun:
                self.execLexem(lex)
            self.genPop("eax")
            self.append("jmp eax")
            self.decIndent()
    def addStrings(self, strs):
        self.strings = strs[:]
    def genFunCall(self, address, regsToSave = []):
        for reg in regsToSave:
            self.genPush(reg)
        label = self.genLabel()
        self.genPush(label)
        self.append("jmp " + address, label + ": nop")
        for reg in regsToSave[::-1]:
            self.genPop(reg)
    def genPush(self, what):
        if (what != "eax"):
            self.append("push eax", "add dword[call_stack_ptr], 4", "mov eax, dword[call_stack_ptr]")
            self.append("mov dword [eax], " + what, "pop eax")
        else:
            self.append("push ebx", "add dword[call_stack_ptr], 4", "mov ebx, dword[call_stack_ptr]")
            self.append("mov dword [ebx], " + what, "pop ebx")

    def genPop(self, where):
        if (where != "eax"):
            self.append("push eax" ,"mov eax, [call_stack_ptr]", 
                        "mov eax, [eax]", "mov " + where + ", eax", "sub dword[call_stack_ptr], 4", "pop eax")
        else:
            self.append("mov eax, [call_stack_ptr]", "mov eax, [eax]", "sub dword[call_stack_ptr], 4")
    def incIndent(self):
        self.indent += 1
    def decIndent(self):
        self.indent -= 1
    def append(self, *strs):
        for s in strs:
            self.s += self.indent*"    " + s + "\n"
    def write(self):
        self.of.write(self.getIncludes())
        self.of.write(self.getDataSection())
        self.of.write(self.getTextSectionHeader())
        self.of.write(self.s)
        self.of.close()

