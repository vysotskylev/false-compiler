#! /usr/bin/python 
from lexical_analysis import Lexem, analyze
from assembler import Assembler
import subprocess
import sys
import os

functions = []
funInd = -1
storage = {}
strings = []

def main():

    usage = "USAGE: {} code.f out".format(sys.argv[0])
    if len(sys.argv) < 2 :
        print usage
        exit(0)
    fname = sys.argv[1]
    
    if len(sys.argv) == 2:
        ind = fname.rfind('.') 
        if ind == -1:
            outFname = fname + '.out'
        else:
            outFname = fname[:ind]
    else:
        outFname = sys.argv[2]
    Compile(fname, outFname)


def Compile(infile, outfile):
    with open(infile, 'r') as f:
        s = f.read()
    
    lexems = analyze(s)
    parse(lexems, 0)

    assemble(lexems, functions, strings, outfile + '.asm')
    subprocess.call(["./build_asm.sh", outfile + '.asm' ])

def parse(lexems, start):
    global funInd, functions
    i = start
    while i < len(lexems):
        if lexems[i].typ == ']':
            funInd += 1
            return (funInd, i)
        elif lexems[i].typ == '[':
            left = i
            fi, right = parse(lexems, i + 1)
            functions.append(lexems[left + 1:right])
            lexems[left:right + 1] = [Lexem('fun', fi)]
        elif lexems[i].typ == 'str':
            strings.append(lexems[i].val)
            lexems[i].val = len(strings) - 1
        i += 1

def assemble(lexems, functions, strings, outFile):
    asm = Assembler(outFile)
    asm.addMain(lexems)
    asm.addFunctions(functions)
    asm.addStrings(strings)
    asm.write()

if __name__ == '__main__':
    main()



