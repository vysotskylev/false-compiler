SECTION .data
str: db '123456789'
print_int_fmt: db '%d', 0
SECTION .text
extern printf
extern getc
global _exit, _print, _read_char

_read_char:
    
;Gets its argument in EAX, returns resulting string in str, its length in EBX
int_to_str:
    push esi
    push edi
    push edx

    mov ebx, 10

    mov esi, str
    cmp eax, 0
    jge .greater
    mov byte [esi], '-'
    inc esi
    neg eax
.greater:
    mov edi,esi
.loop:
    cdq
    div ebx; div 10
    add dl, '0'
    mov byte [esi], dl
    inc esi
    test eax, eax
    jne .loop
    mov ebx, esi
    sub ebx, edi
    dec esi
    ;Now we have reversed string
.revloop:

    mov dl, [esi]
    xchg [edi], dl
    xchg [esi], dl
    inc edi
    dec esi
    cmp esi, edi
    jg .revloop
    
    pop edx
    pop edi
    pop esi
    ret

_exit:
    
	mov	ebx,0		; exit code, 0=normal
	mov	eax,1		; exit command to kernel
	int	0x80		; interrupt 80 hex, call kernel

;_print prints an integer given in the only argument [ebp + 8]
_print:
        push ebp
        mov ebp, esp
        
        push edx
        push ecx
        push ebx
        push eax

        mov eax, [ebp + 8]
        push eax
        push print_int_fmt
        call printf
        add esp, 8
        ;call int_to_str
        
        pop eax
        pop ebx
        pop ecx
        pop edx
        leave

        ret
