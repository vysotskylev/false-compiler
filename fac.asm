%include "io.inc"
SECTION .data
  call_stack_ptr: dd call_stack
  saved_esp: dd 0
SECTION .bss
  storage: resd 26
  call_stack: resd 10000
SECTION .text
global CMAIN
CMAIN:
    mov eax, esp
    mov [saved_esp], eax
    push fun1
    pop eax
    mov dword[storage + 4 * 5], eax
    push 5
    mov eax, dword[storage + 4*5]
    push eax
    pop eax
    push eax
    add dword[call_stack_ptr], 4
    mov eax, dword[call_stack_ptr]
    mov dword [eax], L1
    pop eax
    jmp eax
    L1: nop
    PRINT_DEC 4, [esp]
    pop eax
    xor eax, eax
    mov esp, [saved_esp]
    ret
fun0:
    push dword[esp]
    push 1
    pop eax
    sub dword  [esp], eax
    mov eax, dword[storage + 4*5]
    push eax
    pop eax
    push eax
    add dword[call_stack_ptr], 4
    mov eax, dword[call_stack_ptr]
    mov dword [eax], L2
    pop eax
    jmp eax
    L2: nop
    pop eax
    imul dword  [esp]
    mov dword  [esp], eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun1:
    push dword[esp]
    push 1
    mov edx, 1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    mov edx, 1
    mov ecx, 0
    pop eax
    test eax, eax
    cmovnz edx, ecx
    push edx
    push fun0
    pop edx
    pop ecx
    test ecx, ecx
    jz L3
    add dword[call_stack_ptr], 4
    mov esi, dword[call_stack_ptr]
    mov dword[esi], L3
    jmp edx
    L3: nop
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
