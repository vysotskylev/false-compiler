%include "io.inc"
SECTION .data
  call_stack_ptr: dd call_stack
  saved_esp: dd 0
SECTION .bss
  storage: resd 26
  call_stack: resd 10000
SECTION .text
global CMAIN
CMAIN:
    mov eax, esp
    mov [saved_esp], eax
    push 0
    GET_CHAR eax
    push eax
    push fun0
    push fun1
    pop eax
    pop ebx
    jmp L2
    L1: nop
        push ebx
        add dword[call_stack_ptr], 4
        mov ebx, dword[call_stack_ptr]
        mov dword [ebx], eax
        pop ebx
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], ebx
        pop eax
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], L3
        pop eax
        jmp eax
        L3: nop
        push eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        mov ebx, eax
        sub dword[call_stack_ptr], 4
        pop eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        sub dword[call_stack_ptr], 4
    L2: nop
        push ebx
        add dword[call_stack_ptr], 4
        mov ebx, dword[call_stack_ptr]
        mov dword [ebx], eax
        pop ebx
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], ebx
        pop eax
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], L4
        pop eax
        jmp ebx
        L4: nop
        push eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        mov ebx, eax
        sub dword[call_stack_ptr], 4
        pop eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        sub dword[call_stack_ptr], 4
    pop ecx
    test ecx, ecx
    jnz L1
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 32
    GET_CHAR eax
    push eax
    push fun2
    push fun3
    pop eax
    pop ebx
    jmp L6
    L5: nop
        push ebx
        add dword[call_stack_ptr], 4
        mov ebx, dword[call_stack_ptr]
        mov dword [ebx], eax
        pop ebx
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], ebx
        pop eax
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], L7
        pop eax
        jmp eax
        L7: nop
        push eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        mov ebx, eax
        sub dword[call_stack_ptr], 4
        pop eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        sub dword[call_stack_ptr], 4
    L6: nop
        push ebx
        add dword[call_stack_ptr], 4
        mov ebx, dword[call_stack_ptr]
        mov dword [ebx], eax
        pop ebx
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], ebx
        pop eax
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], L8
        pop eax
        jmp ebx
        L8: nop
        push eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        mov ebx, eax
        sub dword[call_stack_ptr], 4
        pop eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        sub dword[call_stack_ptr], 4
    pop ecx
    test ecx, ecx
    jnz L5
    pop eax
    push fun4
    push fun5
    pop eax
    pop ebx
    jmp L10
    L9: nop
        push ebx
        add dword[call_stack_ptr], 4
        mov ebx, dword[call_stack_ptr]
        mov dword [ebx], eax
        pop ebx
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], ebx
        pop eax
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], L11
        pop eax
        jmp eax
        L11: nop
        push eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        mov ebx, eax
        sub dword[call_stack_ptr], 4
        pop eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        sub dword[call_stack_ptr], 4
    L10: nop
        push ebx
        add dword[call_stack_ptr], 4
        mov ebx, dword[call_stack_ptr]
        mov dword [ebx], eax
        pop ebx
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], ebx
        pop eax
        push eax
        add dword[call_stack_ptr], 4
        mov eax, dword[call_stack_ptr]
        mov dword [eax], L12
        pop eax
        jmp ebx
        L12: nop
        push eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        mov ebx, eax
        sub dword[call_stack_ptr], 4
        pop eax
        mov eax, [call_stack_ptr]
        mov eax, [eax]
        sub dword[call_stack_ptr], 4
    pop ecx
    test ecx, ecx
    jnz L9
    push 32
    PRINT_CHAR [esp]
    pop eax
    xor eax, eax
    mov esp, [saved_esp]
    ret
fun0:
    push dword[esp]
    push 48
    pop eax
    pop ebx
    push eax
    push ebx
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    pop eax
    pop ebx
    push eax
    push ebx
    push dword[esp]
    push 57
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    pop eax
    pop ebx
    pop ecx
    push ebx
    push eax
    push ecx
    pop eax
    or dword[esp], eax
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun1:
    push 48
    pop eax
    sub dword  [esp], eax
    pop eax
    pop ebx
    push eax
    push ebx
    push 10
    pop eax
    imul dword  [esp]
    mov dword  [esp], eax
    pop eax
    add dword  [esp], eax
    GET_CHAR eax
    push eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun2:
    push dword[esp]
    push 32
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun3:
    GET_CHAR eax
    push eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun4:
    push dword[esp]
    push 32
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun5:
    PRINT_CHAR [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
