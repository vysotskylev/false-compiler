from  curses.ascii import * 
class CharStream:
    def __init__(self, s):
        self.s = s
        self.i = 0
        self.length = len(s)
    def peek(self):
        return self[self.i]
    def get(self):
        if self.i == self.length:
            return None
        self.i += 1
        return self.s[self.i-1]

class Lexem:
    def __init__(self, typ, val = None):
        self.typ = typ
        self.val = val
    def toString(self):
        return 'Type: {},  Value: {}'.format(self.typ, self.val)
    def Print(self):
        print self.toString()

class ParseError(Exception) :
    def __init__(self, msg):
        self.message = msg
    def __repr__(self):
        return self.message

class Automata:
    def __init__(self, stream):
        self.stream = stream
        self.lexems = []
        self.simple = r"[]" + r"+-*/_" + r"$@\%" + r">=&|~" + r".,^" + r"?#!"
        self.c = stream.get()
    def get(self):
        self.c = self.stream.get()
    def Init(self):
        if self.c is None:
            return
        if self.c.isspace():
            self.get()
            self.Init()
        elif isalpha(self.c):
            if islower(self.c):
                self.Variable()
            elif self.c in "OB":
                self.lexems.append(Lexem(self.c))
                self.get()
                self.Init()
            else:
                raise ParseError('Unknown symbol: it should be either lowercase variable or O or B')
        elif isdigit(self.c):
            self.Number()
        elif self.c == "'":
            self.Char()
        elif self.c == '"':
            self.String()
        elif self.c == '{':
            self.Commentary()
        else:
            self.Simple()
    def Variable(self):
        var = self.c
        self.get()
        if self.c == ';':
            self.lexems.append(Lexem('varGet',var))
        elif self.c == ':':
            self.lexems.append(Lexem('varSet', var))
        else:
            raise ParseError('Unknown operation with variable (it must be either : or ;)')
        self.get()
        self.Init()
    def Number(self):
        numStr = self.c
        while True:
            self.get()
            if isdigit(self.c):
                numStr += self.c
            else:
                break
        self.lexems.append(Lexem('number',  numStr))
        self.Init()
    def Char(self):
        self.get()
        self.lexems.append(Lexem('char', self.c))
        self.get()
        self.Init()
    def String(self):
        self.get()
        s = ""
        while self.c is not None and self.c != '"':
            s += self.c
            self.get()
        if self.c is None:
            raise ParseError('Unclosed quote (") ')
        self.lexems.append(Lexem('str', s))
        self.get()
        self.Init()
    def Commentary(self):
        self.get()
        while self.c is not None and self.c != '}':
            self.get()
        if self.c is None:
            raise ParseError('Unclosed commentary ({) ')
        self.get()
        self.Init()
    def Simple(self):
        if self.c in self.simple:
            self.lexems.append(Lexem(self.c))
            self.get()
        else:
            raise ParseError('Unknown operation ({})'.format(self.c))
        self.Init()

    def run(self):
        self.Init()
        return self.lexems;

def analyze(s):
    stream = CharStream(s)
    try:
        a = Automata(stream)
        return a.run()
    except ParseError as err:
        print s
        print (stream.i - 1) * ' ' + '^'
        print "ParseError: " +  err.message 
        exit()
