%include "io.inc"
SECTION .data
call_stack_ptr: dd call_stack
saved_esp: dd 0
SECTION .bss
storage: resd 26
call_stack: resd 10000
SECTION .text
global CMAIN
CMAIN:
    mov eax, esp
    mov [saved_esp], eax
    push fun0
    pop eax
    mov dword[storage + 4 * 8], eax
    push 10
    mov eax, dword[storage + 4*8]
    push eax
    pop eax
    push eax
    add dword[call_stack_ptr], 4
    mov eax, dword[call_stack_ptr]
    mov dword [eax], L1
    pop eax
    jmp eax
    L1: nop
    PRINT_DEC 4, [esp]
    pop eax
    xor eax, eax
    mov esp, [saved_esp]
    ret
fun0:
    push 1
    pop eax
    add dword  [esp], eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
