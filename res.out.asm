%include "io.inc"
SECTION .data
  call_stack_ptr: dd call_stack
  saved_esp: dd 0
SECTION .bss
  storage: resd 26
  call_stack: resd 10000
SECTION .text
global CMAIN
CMAIN:
    mov eax, esp
    mov [saved_esp], eax
    push 1
    push 2
    push 3
    pop eax
    pop ebx
    pop ecx
    push ebx
    push eax
    push ecx
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 10
    PRINT_CHAR [esp]
    pop eax
    push 2
    push 1
    push dword[esp]
    push dword[esp]
    PRINT_DEC 4, [esp]
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    push 10
    PRINT_CHAR [esp]
    pop eax
    push 34
    push 56
    pop eax
    pop ebx
    push eax
    push ebx
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 10
    PRINT_CHAR [esp]
    pop eax
    push 1
    push 2
    push 3
    pop eax
    pop eax
    PRINT_DEC 4, [esp]
    pop eax
    push 10
    PRINT_CHAR [esp]
    pop eax
    push 1
    push 2
    push 3
    push 1
    pop eax
    push dword [esp + eax]
    PRINT_DEC 4, [esp]
    pop eax
    push 10
    PRINT_CHAR [esp]
    pop eax
    xor eax, eax
    mov esp, [saved_esp]
    ret
