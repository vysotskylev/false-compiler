%include "io.inc"
SECTION .data
  call_stack_ptr: dd call_stack
  saved_esp: dd 0
SECTION .bss
  storage: resd 26
  call_stack: resd 10000
SECTION .text
global CMAIN
CMAIN:
    mov eax, esp
    mov [saved_esp], eax
    push 5
    GET_CHAR eax
    push eax
    push 48
    pop eax
    sub dword  [esp], eax
    pop eax
    pop ebx
    push eax
    push ebx
    push 10
    pop eax
    imul dword  [esp]
    mov dword  [esp], eax
    pop eax
    add dword  [esp], eax
    PRINT_DEC 4, [esp]
    pop eax
    xor eax, eax
    mov esp, [saved_esp]
    ret
