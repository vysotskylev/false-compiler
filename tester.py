#! /usr/bin/python 
import compiler
import subprocess
import os, sys

def test(src, inp, out):
    compiler.Compile(src, 'tmp_exe')
    progIn = open(inp, 'r')
    progOut = open('tmp_out', 'w')
    subprocess.call('./tmp_exe', stdin = progIn, stdout = progOut)
    progOut.close()
    with open('tmp_out', 'r') as f:
        progOutStr = f.read().strip()
    with open(out, 'r') as f:
        realOutStr = f.read().strip()
    os.remove('tmp_exe')
    os.remove('tmp_out')

    if progOutStr != realOutStr:
        return (progOutStr, realOutStr)

def main():
    if len(sys.argv) < 2:
        print "USAGE: {} test_path".format(sys.argv[0])
    progPath = sys.argv[1] + '/progs/'
    insPath = sys.argv[1] + '/ins/'
    outsPath = sys.argv[1] + '/outs/'

    delim = '=' * 80
    tests = os.listdir(progPath)
    print delim
    for num, t in enumerate(tests):
        diff = test(progPath + t, insPath + t, outsPath + t)
        if diff is None:
            print "Test \"{}\" passed!".format(t)
        else:
            print "Test \"{}\" NOT passed!\nExpected:\n{} \nFound:\n{}".format(t, diff[1], diff[0])
        print delim

if __name__ == '__main__':
    main()
