%include "io.inc"
SECTION .data
  call_stack_ptr: dd call_stack
  saved_esp: dd 0
  str0 : db "Goodbye!",0 
SECTION .bss
  storage: resd 26
  call_stack: resd 10000
SECTION .text
global CMAIN
CMAIN:
    mov eax, esp
    mov [saved_esp], eax
    push 10
    push 11
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    not dword[esp]
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 7
    push 3
    pop eax
    add dword  [esp], eax
    push 9
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 2
    push 2
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 7
    push 7
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 7
    push 7
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    push 3
    push 4
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    pop eax
    and dword[esp], eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 1
    neg dword [esp]
    push 1
    pop eax
    add dword  [esp], eax
    push 0
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    push 3
    push 4
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    pop eax
    or dword[esp], eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 1
    push 0
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    push 5
    push 6
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    pop eax
    or dword[esp], eax
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    push 3
    push 4
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    not dword[esp]
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    xor eax, eax
    mov esp, [saved_esp]
    ret
fun0:
    push dword[esp]
    push 48
    pop eax
    pop ebx
    push eax
    push ebx
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    pop eax
    pop ebx
    push eax
    push ebx
    push dword[esp]
    push 57
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    pop eax
    pop ebx
    pop ecx
    push ebx
    push eax
    push ecx
    pop eax
    or dword[esp], eax
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun1:
    push 48
    pop eax
    sub dword  [esp], eax
    pop eax
    pop ebx
    push eax
    push ebx
    push 10
    pop eax
    imul dword  [esp]
    mov dword  [esp], eax
    pop eax
    add dword  [esp], eax
    GET_CHAR eax
    push eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun2:
    push dword[esp]
    push 32
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun3:
    GET_CHAR eax
    push eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun4:
    push dword[esp]
    push 32
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun5:
    PRINT_CHAR [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun6:
    pop eax
    pop ebx
    push eax
    push ebx
    pop eax
    push 1
    pop eax
    pop ebx
    push eax
    push ebx
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun7:
    push dword[esp]
    push 1
    pop eax
    sub dword  [esp], eax
    mov eax, dword[storage + 4*5]
    push eax
    pop eax
    push eax
    add dword[call_stack_ptr], 4
    mov eax, dword[call_stack_ptr]
    mov dword [eax], L1
    pop eax
    jmp eax
    L1: nop
    pop eax
    imul dword  [esp]
    mov dword  [esp], eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun8:
    push dword[esp]
    push 1
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    push dword[esp]
    push fun6
    pop edx
    pop ecx
    test ecx, ecx
    jz L2
    add dword[call_stack_ptr], 4
    mov esi, dword[call_stack_ptr]
    mov dword[esi], L2
    jmp edx
    L2: nop
    not dword[esp]
    push fun7
    pop edx
    pop ecx
    test ecx, ecx
    jz L3
    add dword[call_stack_ptr], 4
    mov esi, dword[call_stack_ptr]
    mov dword[esi], L3
    jmp edx
    L3: nop
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun9:
    push 1
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun10:
    push 0
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun11:
    push 1
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun12:
    push 0
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun13:
    push 1
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun14:
    push 0
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun15:
    push 1
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun16:
    push 0
    PRINT_DEC 4, [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun17:
    push 1
    pop eax
    sub dword  [esp], eax
    push dword[esp]
    push 0
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp ebx, eax
    cmovg ecx, edx
    push ecx
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun18:
    pop eax
    pop ebx
    push eax
    push ebx
    push dword[esp]
    pop eax
    add dword  [esp], eax
    pop eax
    pop ebx
    push eax
    push ebx
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun19:
    push 1
    pop eax
    sub dword  [esp], eax
    push dword[esp]
    push 0
    mov edx, -1
    mov ecx, 0
    pop eax
    pop ebx
    cmp eax, ebx
    cmove ecx, edx
    push ecx
    not dword[esp]
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
fun20:
    push dword[esp]
    PRINT_DEC 4, [esp]
    pop eax
    push 32
    PRINT_CHAR [esp]
    pop eax
    mov eax, [call_stack_ptr]
    mov eax, [eax]
    sub dword[call_stack_ptr], 4
    jmp eax
